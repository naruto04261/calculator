程式演算法和功能說明:<br/>
(i) 利用regex，先取出每層括號的內容，進行計算，將括號內的文字替換成計算結果，再將剩餘結果以先乘除後加減的計算原則，算出答案。<br/>
(ii) 所算出的答案，可做為下個算式的開頭。<br/>
(iii) 除了可以用螢幕上的按鍵輸入外，也可以用鍵盤輸入。<br/>
(iv) 結果計算函式名稱: calc<br/>
(v)	計算先乘除，後加減的函式: arrange_calc<br/>
<br/>
主程式位置: https://gitlab.com/naruto04261/calculator/-/blob/master/106303007_HW1/Form1.cs