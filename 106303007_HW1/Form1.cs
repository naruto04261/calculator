﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Hw1_106303007
{
    public partial class Form1 : Form
    {
        static char text_input;
        static int pre_len;
        static Boolean key_flag = true, flag = true, calc_flag = true, press_flag;
        static MatchCollection mc0, mc1, mc2, mc6, mc7, mc8;
        static string p0 = @"([\(\)])", p1 = @"\(", p2 = @"\)",
                      p3 = @"([\+\-])", p4 = @"([\*\/])",
                      p5 = @"([\*\/\+\-])", p6 = @"[\.]",
                      p7 = @"[\+\-\*\/]\.", p8 = @"[\+\-\*\/][\)\=]";
        static Regex r0, r1, r2, r3, r4, r5, r6, r7, r8;
        static string[] str5;
        public Form1()
        {
            InitializeComponent();
            r0 = new Regex(p0, RegexOptions.IgnoreCase);
            r1 = new Regex(p1, RegexOptions.IgnoreCase);
            r2 = new Regex(p2, RegexOptions.IgnoreCase);
            r3 = new Regex(p3, RegexOptions.IgnoreCase);
            r4 = new Regex(p4, RegexOptions.IgnoreCase);
            r5 = new Regex(p5, RegexOptions.IgnoreCase);
            r6 = new Regex(p6, RegexOptions.IgnoreCase);
            r7 = new Regex(p7, RegexOptions.IgnoreCase);
            r8 = new Regex(p8, RegexOptions.IgnoreCase);
        }
        private static string arrange_calc(string str)
        {
            StringBuilder str_tp = new StringBuilder(str);
            int length = str.Length;
            char operator_save = 'N';
            Boolean change_op = false;
            for (int i = length - 1; i >= 0; i--)
            {
                switch (str_tp[i])
                {
                    case '+':
                        operator_save = '+';
                        if (change_op)
                        {
                            str_tp[i] = '-';
                            operator_save = '-';
                            change_op = false;
                        }
                        break;
                    case '-':
                        if (operator_save == '-' )
                        {
                            str_tp.Remove(i + 1, 1);
                            change_op = true;
                        }
                        operator_save = '-';
                        if (change_op)
                        {
                            str_tp[i] = '+';
                            change_op = false;
                            operator_save = '+';
                        }
                        break;
                    case '*':
                        if (operator_save == '-' || operator_save == '+')
                        {
                            str_tp.Remove(i + 1, 1);
                            i--;
                            change_op = true;
                        }
                        operator_save = '*';
                        break;
                    case '/':
                        if (operator_save == '-' || operator_save == '+')
                        {
                            str_tp.Remove(i + 1, 1);
                            i--;
                            change_op = true;
                        }
                        operator_save = '/';
                        break;
                    default:
                        operator_save = 'N';
                        break;
                }
            }
            if (change_op)
            {
                str = "-" + str_tp.ToString();
                change_op = false;
            }
            else
            {
                str = str_tp.ToString();
            }
            return str;
        }
        private static string calc(string str)
        {
            double calc_result;
            string calc_text;
            int n1 = 0, n2 = 0, sum;
            while (str.IndexOf(')') != -1)
            {
                str = arrange_calc(str);
                mc0 = r0.Matches(str);
                int pre_index = 0;
                string pre_value = "none";
                foreach (Match match in mc0)
                {
                    if (pre_value == "(" && match.Value == ")")
                    {
                        n1 = pre_index;
                        n2 = match.Index;
                        break;
                    }
                    pre_value = match.Value;
                    pre_index = match.Index;
                }
                sum = str.Length;
                calc_text = str.Substring(n1 + 1, n2 - n1 - 1);
                calc_result = calc_tp(calc_text);
                str = str.Remove(n1) + calc_result.ToString("g") + str.Substring(n2 + 1, sum - n2 - 1);
            }
            str = arrange_calc(str);
            return calc_tp(str).ToString("g");
        }
        private static double calc_tp(string str) {
            string[] op1, op2;
            double result1 = 0, result2 = 0;
            int mode1 = 0, mode2 = 0;
            op1 = r3.Split(str);
            foreach (string value in op1)
            {
                switch (value)
                {
                    case "+":
                        mode2 = 0;
                        break;
                    case "-":
                        mode2 = 1;
                        break;
                    case "":
                        result1 = 0;
                        break;
                    default:
                        op2 = r4.Split(value);
                        mode1 = 0;
                        result1 = 0;
                        foreach (string value1 in op2)
                        {
                            switch (value1)
                            {
                                case "*":
                                    mode1 = 1;
                                    break;
                                case "/":
                                    mode1 = 2;
                                    break;
                                case "":
                                    break;
                                default:
                                    if (mode1 == 0)
                                    {
                                        result1 += Double.Parse(value1);
                                    }
                                    else if (mode1 == 1)
                                    {
                                        result1 = result1 * Double.Parse(value1);
                                    }
                                    else
                                    {
                                        result1 = result1 / Double.Parse(value1);
                                    }
                                    break;
                            }
                        }
                        if (mode2 == 0)
                        {
                            result2 += result1;
                        }
                        else if (mode2==1)
                        { 
                            result2 -= result1;
                        }
                        break;
                }
            }
            return result2;
        }
        private void label1_TextChanged(object sender, EventArgs e)
        {
            if (press_flag)
            {
                press_flag = false;
                if (calc_flag)
                {
                    int length = label1.Text.Length;
                    if (flag)
                    {
                        if (length != 0)
                        {
                            text_input = label1.Text[length - 1];
                        }
                        if (length >= pre_len)
                        {
                            switch (text_input)
                            {
                                case '1':
                                case '2':
                                case '3':
                                case '4':
                                case '5':
                                case '6':
                                case '7':
                                case '8':
                                case '9':
                                case '0':
                                case '(':
                                    if (length == 2 && label1.Text[0] == '0')
                                    {
                                        label1.Text = text_input.ToString();
                                    }
                                    break;
                                case ')':
                                    mc1 = r1.Matches(label1.Text);
                                    mc2 = r2.Matches(label1.Text);
                                    if (mc1.Count < mc2.Count)
                                    {
                                        label1.Text = "(" + label1.Text;
                                    }
                                    break;
                                case '.':
                                    mc7 = r7.Matches(label1.Text);
                                    if (mc7.Count > 0)
                                    {
                                        label1.Text = label1.Text.Remove(length - 1);
                                    }
                                    else
                                    {
                                        str5 = r5.Split(label1.Text);
                                        foreach (string value in str5)
                                        {
                                            mc6 = r6.Matches(value);
                                            if (mc6.Count > 1)
                                            {
                                                label1.Text = label1.Text.Remove(length - 1);
                                            }
                                        }
                                    }
                                    break;
                                case '+':
                                case '-':
                                case '*':
                                case '/':
                                    if (length == 2 && text_input == '-' && (label1.Text[0] == '0' || label1.Text[0] == '-') && label1.Text[1] == '-')
                                    {
                                        label1.Text = "-";
                                    }
                                    else if (length >= 2)
                                    {

                                        switch (label1.Text[length - 2])
                                        {
                                            case '+':
                                            case '-':
                                            case '*':
                                            case '/':
                                                label1.Text = label1.Text.Remove(length - 2) + text_input.ToString();
                                                break;
                                        }
                                    }
                                    break;
                                case '=':
                                    mc1 = r1.Matches(label1.Text);
                                    mc2 = r2.Matches(label1.Text);
                                    if (mc1.Count < mc2.Count)
                                    {
                                        label1.Text = "(" + label1.Text;
                                    }
                                    else if (mc1.Count > mc2.Count)
                                    {
                                        int i;
                                        label1.Text = label1.Text.Remove(length - 1);
                                        for (i = 0; i < mc1.Count - mc2.Count; i++)
                                        {
                                            label1.Text += ")";
                                        }
                                        label1.Text += "=";
                                    }
                                    length = label1.Text.Length;
                                    if (label1.Text.IndexOf(")(") != -1)
                                    {
                                        label1.Text = "Syntax error";
                                        flag = false;
                                    }
                                    else
                                    {
                                        calc_flag = false;
                                        mc8 = r8.Matches(label1.Text);
                                        if (mc8.Count > 0)
                                        {
                                            label1.Text = "Syntax error";
                                            flag = false;
                                            calc_flag = true;
                                        }
                                        else
                                        {
                                            if (label1.Text.IndexOf("=") != -1)
                                            {
                                                label1.Text = label1.Text.Remove(length - 1);
                                            }
                                            try
                                            {
                                                label1.Text = calc(label1.Text);
                                            }
                                            catch
                                            {
                                                label1.Text = "Syntax error";
                                                flag = false;
                                            }
                                            calc_flag = true;
                                        }
                                    }
                                    press_flag = true;
                                    break;
                            }
                        }
                        pre_len = label1.Text.Length;
                    }
                    else
                    {
                        label1.Text = "Syntax error";
                    }
                }
            }
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            press_flag = true;
            label1.Text += "1";
        }
        private void Button2_Click(object sender, EventArgs e)
        {
            press_flag = true;
            label1.Text += "2";
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            press_flag = true;
            label1.Text += "3";
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            press_flag = true;
            label1.Text += "4";
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            press_flag = true;
            label1.Text += "5";
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            press_flag = true;
            label1.Text += "6";
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            press_flag = true;
            label1.Text += "7";
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            press_flag = true;
            label1.Text += "8";
        }

        private void Button9_Click(object sender, EventArgs e)
        {
            press_flag = true;
            label1.Text += "9";
        }

        private void Button11_Click(object sender, EventArgs e)
        {
            press_flag = true;
            label1.Text += "0";
        }

        private void Button10_Click(object sender, EventArgs e)
        {
            press_flag = true;
            flag = true;
            label1.Text = "0";
            key_flag = true;
            pre_len = 1;
        }

        private void Button12_Click(object sender, EventArgs e)
        {
            press_flag = true;
            label1.Text += ".";
        }

        private void Button13_Click(object sender, EventArgs e)
        {
            press_flag = true;
            label1.Text += "+";
        }

        private void Button14_Click(object sender, EventArgs e)
        {
            press_flag = true;
            label1.Text += "-";
        }

        private void Button15_Click(object sender, EventArgs e)
        {
            press_flag = true;
            label1.Text += "*";
        }

        private void Button16_Click(object sender, EventArgs e)
        {
            press_flag = true;
            label1.Text += "/";
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            press_flag = true;
            key_flag = true;
        }
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (key_flag)
            {
                string keypress = (e.KeyData).ToString();
                press_flag = true;
                switch (keypress)
                {
                    case "Back":
                        int length = label1.Text.Length;
                        if (length > 1)
                        {
                            label1.Text = label1.Text.Remove(length - 1);
                        }else if(length == 1)
                        {
                            label1.Text = "0";
                        }
                        break;
                    case "D1":
                    case "D2":
                    case "D3":
                    case "D4":
                    case "D5":
                    case "D6":
                    case "D7":
                    case "D8":
                    case "D9":
                    case "D0":
                        label1.Text += keypress[1];
                        break;
                    case "NumPad1":
                    case "NumPad2":
                    case "NumPad3":
                    case "NumPad4":
                    case "NumPad5":
                    case "NumPad6":
                    case "NumPad7":
                    case "NumPad8":
                    case "NumPad9":
                    case "NumPad0":
                        label1.Text += keypress[6];
                        break;
                    case "ShiftKey, Shift":
                        break;
                    case "D9, Shift":
                        label1.Text += '(';
                        break;
                    case "D0, Shift":
                        label1.Text += ')';
                        break;
                    case "Add":
                    case "Oemplus, Shift":
                        label1.Text += "+";
                        key_flag = false;
                        break;
                    case "OemMinus":
                    case "Subtract":
                        label1.Text += "-";
                        key_flag = false;
                        break;
                    case "D8, Shift":
                    case "Multiply":
                        label1.Text += "*";
                        key_flag = false;
                        break;
                    case "OemQuestion":
                    case "Divide":
                        label1.Text += "/";
                        key_flag = false;
                        break;
                    case "Oemplus":
                        label1.Text += "=";
                        key_flag = false;
                        break;
                    case "Decimal":
                    case "OemPeriod":
                        label1.Text += ".";
                        key_flag = false;
                        break;
                    default:
                        press_flag = false;
                        key_flag = false;
                        break;
                }
            }
        }

        private void Button18_Click(object sender, EventArgs e)
        {
            press_flag = true;
            int length = label1.Text.Length;
            if (length > 1)
            {
                label1.Text = label1.Text.Remove(length - 1);
            }
            else if (length == 1)
            {
                label1.Text = "0";
            }
        }

        private void Button19_Click(object sender, EventArgs e)
        {
            press_flag = true;
            label1.Text += "(";
        }

        private void Button20_Click(object sender, EventArgs e)
        {
            press_flag = true;
            label1.Text += ")";
        }

        private void Button17_Click(object sender, EventArgs e)
        {
            press_flag = true;
            label1.Text += "=";
        }
    }
}
